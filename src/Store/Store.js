import axios from 'axios'
import localforage from 'localforage'
import isNull from 'lodash.isnull'

let SITE_API = '/'
if (process.env.NODE_ENV === 'development') {
  SITE_API = 'http://localhost:9090'
} else {
  SITE_API = 'http://workshop.istwire.com/api'
}

class Store {
  static getResource (resource) {
    const requestResource = () => {
      return new Promise((resolve) => {
        axios
          .get(`${SITE_API}/${resource}`)
          .then((response) => {
            return resolve(response.data)
          })
      })
    }

    return new Promise((resolve) => {
      // ищем в кеше
      localforage
        .getItem(resource)
        .then((data) => {
          // если есть в кеше, то сразу
          // возвращаем из кеша
          if (!isNull(data)) {
            resolve(data)
          }

          // обновляем кеш
          requestResource()
            .then((data) => {
              // кешируем/обновляем кеш
              localforage
                .setItem(resource, data)

              resolve(data)
            })
        })
    })
  }

  static clear () {
    localforage.removeItem('products')
  }

  static products () {
    return new Promise((resolve) => {
      Store
        .getResource('products')
        .then((products) => {
          resolve(products)
        })
    })
  }

  static product (id) {
    return new Promise((resolve) => {
      Store
        .getResource('products')
        .then((products) => {
          resolve(products[id])
        })
    })
  }

  static cart () {
    return new Promise((resolve) => {
      localforage
        .getItem('cart')
        .then((data) => {
          const cart = !isNull(data) ? data : {items: {}}

          resolve(cart)
        })
    })
  }

  static addToCart (product, quantity = 1, variation) {
    const getProductSimple = () => {
      return {
        [product.id]: {
          'product_id': product.id,
          'variation_id': '',
          title: product.title,
          price: product.price,
          categories: product.categories,
          property: '',
          quantity
        }
      }
    }

    const getProperty = (product) => {
      let property = []
      for (let name in product.attributes) {
        let option = product.attributes[name]
        property.push(`${name}: ${option}`)
      }
      return property
    }

    const getProductVariation = () => {
      return {
        [variation.id]: {
          'product_id': product.id,
          'variation_id': variation.id,
          title: product.title,
          price: variation.price,
          categories: product.categories,
          property: getProperty(variation),
          quantity
        }
      }
    }

    const getProduct = () => {
      if (product.type === 'simple') {
        return getProductSimple()
      }

      if (product.type === 'variable') {
        return getProductVariation()
      }
    }

    const updateCart = (cart) => {
      return new Promise((resolve) => {
        const product = getProduct()
        const id = Object.keys(product)[0]

        if (cart.items[id]) {
          product[id].quantity += cart.items[id].quantity
        }

        cart.items = Object.assign(cart.items, product)

        localforage
          .setItem('cart', cart)

        resolve(cart)
      })
    }

    return new Promise((resolve) => {
      Store.cart()
        .then((cart) => {
          updateCart(cart)
            .then((cart) => {
              resolve(cart)
            })
        })
    })
  }

  static setCart (cart) {
    return new Promise((resolve) => {
      localforage
        .setItem('cart', cart)
    })
  }

  static clearCart () {
    return new Promise(() => {
      localforage
        .removeItem('cart')
    })
  }

  static getItems () {
    return new Promise((resolve) => {
      Store
        .cart()
        .then((cart) => {
          const items = []
          for (let key in cart.items) {
            let item = cart.items[key]
            items.push({
              'product_id': item['product_id'],
              'quantity': item.quantity,
              'variation_id': item['variation_id']
            })
          }
          return resolve(items)
        })
    })
  }

  static categories () {
    return new Promise((resolve) => {
      Store
        .getResource('categories')
        .then((products) => {
          resolve(products)
        })
    })
  }

  static createOrder (order) {
    return new Promise((resolve) => {
      const data = new window.FormData()
      data.append('order', JSON.stringify(order))
      axios
        .post(SITE_API + '/order', data)
        .then((response) => {
          resolve(response)
        })
    })
  }

  static getUser (phone) {
    return new Promise((resolve) => {
      // ищем в кеше
      localforage
        .getItem(`user/${phone}`)
        .then((data) => {
          if (!isNull(data)) {
            resolve(data)
          }

          const fields = new window.FormData()
          fields.append('phone', phone)
          axios
            .post(`${SITE_API}/user`, fields)
            .then((response) => {
              // кешируем
              localforage
                .setItem(`user/${phone}`, response.data)

              return resolve(response.data)
            })
        })
    })
  }

  static saveOrder (order) {
    localforage
      .setItem('order', order)
  }

  static getOrder (order) {
    return new Promise((resolve) => {
      localforage
        .getItem('order')
        .then((order) => {
          resolve(order)
        })
    })
  }
}

export default Store
