import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import routes from './config/routes'
import VueMaterial from 'vue-material'
import Inputmask from 'jquery.inputmask/dist/inputmask/inputmask'
import './styles/common.scss'
import 'vue-material/dist/vue-material.css'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueMaterial)

Vue.material.theme.register('default', {
  primary: 'cyan',
  accent: 'pink'
})

const router = new VueRouter({
  routes
})

Vue.directive('input-mask', {
  params: ['mask'],
  bind: (el, binding) => {
    Inputmask({mask: binding.value}).mask(el)
  }
})

/* eslint-disable no-new */
new Vue({
  router
}).$mount('#app')

